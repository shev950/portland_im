$(document).ready(function () {

    const els = {
            accordLi : $('nav').find('.accord'),
            chevRight : '<i class="fas fa-chevron-right button-right" aria-hidden="true"></i>' ,
            chevDown : '<i class="fas fa-chevron-down button-right" aria-hidden="true"></i>'
        } ,
        speed = 500 ;

    els.accordLi.on('click', function(){
        const self = $(this),
            selfIcon = self.children('i'),
            collapsedMenu = self.find('ul');

        self.toggleClass('slide');

        if(self.hasClass('slide')){
            collapsedMenu.slideDown(speed);
            selfIcon.replaceWith(els.chevDown);
            self
                .addClass('active-sub-list-item')
                .siblings()
                .removeClass('active-sub-list-item');
            self
                .siblings()
                .find('ul')
                .slideUp(speed);
            self
                .siblings('li')
                .children('i')
                .replaceWith(els.chevRight);
        }

        else {
            collapsedMenu.slideUp(speed);
            selfIcon.replaceWith(els.chevRight);
            self.addClass('active-sub-list-item');
        }
    });

    (function($) {
        $(function() {

            $('input, select').styler();
            $('#nav-toggle').styler('destroy');
        });
    })(jQuery);

});

document.addEventListener('DOMContentLoaded', function(){
    var checkbox = document.querySelector('#nav-toggle');
    var checkboxLabel = document.querySelector('#nav-toggle-label');

    function removeActive(){
        checkboxLabel.classList.remove('nav-toggle-label--active');
    }

    checkbox.onclick = function(){
        if (checkbox.checked){
            checkboxLabel.classList.add('nav-toggle-label--active');
        } else{
            removeActive();
        }
    };

    var menu = document.querySelector('.menu');
    menu.onclick = function(){
        removeActive();
        checkbox.checked = false;
    };
});

var swiper = new Swiper('.swiper-container', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
    },
});